﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public int timer = 100;
    public Text countdownText;

    void Start()
    {
        //Time.timeScale = 0f;
        StartCoroutine("LobbyTime");
    }

    void Update()
    {
        countdownText.text = ("Waiting for Player = " + timer);

        if (timer <= 0)
        {
            StopCoroutine("LobbyTime");
            countdownText.text = "GO!!!";
            //Destroy(this.gameObject);
            //Time.timeScale = 1f;
        }
        
    }

    IEnumerator LobbyTime()
    {
       while (true)
        {
            yield return new WaitForSeconds(1);
            timer--;
        }
    }
}
