﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Randomizing : MonoBehaviour {

    public Sprite[] sprite;
    private SpriteRenderer ren;
    int random;

    void Start()
    {
        ren = GetComponent<SpriteRenderer>();
        random = Random.Range(0, 2);
        ren.sprite = sprite[random];
    }
}
