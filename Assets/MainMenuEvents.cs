﻿using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenuEvents : MonoBehaviour
{
    private Text loginButtonText;
    private Text authStatus;
    // ... other code here... 
    public void Start()
    {
        GameObject startButton = GameObject.Find("startButton");
        EventSystem.current.firstSelectedGameObject = startButton;
        loginButtonText = GameObject.Find("Play").GetComponentInChildren<Text>();
        authStatus = GameObject.Find("AuthStatus").GetComponent<Text>();

        //  ADD THIS CODE BETWEEN THESE COMMENTS

        // Create client configuration
        PlayGamesClientConfiguration config = new
            PlayGamesClientConfiguration.Builder()
            .Build();

        // Enable debugging output (recommended)
        PlayGamesPlatform.DebugLogEnabled = true;

        // Initialize and activate the platform
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        // END THE CODE TO PASTE INTO START

        PlayGamesPlatform.Instance.Authenticate(LoginCallBack, true);
    }

    public void Login()
    {
        if(!PlayGamesPlatform.Instance.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.Authenticate(LoginCallBack, false);
        }
        else
        {
            PlayGamesPlatform.Instance.SignOut();
        }
        loginButtonText.text = "Sign In";
        authStatus.text = "";
    }

    public void LoginCallBack(bool success)
    {
        if (success)
        {
            Debug.Log("(Player) Signed in!");

            // Change sign-in button text
            loginButtonText.text = "Sign out";

            // Show the user's name
            authStatus.text = "Signed in as: " + Social.localUser.userName;
        }
        else
        {
            Debug.Log("(Player) Sign-in failed...");

            // Show failure message
            loginButtonText.text = "Sign in";
            authStatus.text = "Sign-in failed";
        }
    }

    // ...
}